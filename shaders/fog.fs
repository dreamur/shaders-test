#version 130

uniform float time;
#define desiredColor vec3(1.0, 1.0, 1.0)

void main()
{
    float opacity = clamp(abs(cos(time / 5) * cos(time / 5)), 0.0, 0.8);
    gl_FragColor = vec4(desiredColor, opacity);
}
