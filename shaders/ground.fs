#version 130

uniform vec3 desiredColor;
uniform float shadowOrHilight;

void main()
{
    vec3 shadow = abs( (shadowOrHilight - gl_TexCoord[0].yyy) / 6.0 );
    vec3 color =  mix(desiredColor, shadow, 0.5);
    gl_FragColor = vec4(color, 1.0);
}
