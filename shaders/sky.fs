#version 130

uniform vec3 desiredColor;
uniform float smoothLowerBound;
uniform float shadowOrHilight;

void main()
{
    vec3 color = abs( (shadowOrHilight - gl_TexCoord[0].yyy) / 1.5 );
    color = smoothstep(vec3(smoothLowerBound), vec3(1.0), color.yyy - .2 );
    color = mix (desiredColor, color, 0.5);
    gl_FragColor = vec4(color, 1.0);
}
