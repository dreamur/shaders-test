#version 130

uniform vec3 desiredColor;

void main()
{
    vec3 color = desiredColor * (1.0 - gl_TexCoord[0].yyy);
    gl_FragColor = vec4(color , 1.0);
}
