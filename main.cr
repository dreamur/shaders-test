# NOTE: RenderTexture objects are being overused
#       this is causing heavy load on the system

require "crsfml"

WINDOW_W = 800
WINDOW_H = 600

ZOOM_THESH = 15
ZOOM_HORIZON_THRESH = ZOOM_THESH // 3

struct SF::Vector3(T)

  def >=(other)
    rt = self.x >= other.x && self.y >= other.y && self.z >= other.z
    rt
  end

  def <=(other)
    rt = self.x <= other.x && self.y <= other.y && self.z <= other.z
    rt
  end

end

# defining this outside of shaders since the shapes' colors are uniforms
def mix(firstColor : SF::Vector3f, secondColor : SF::Vector3f, percent : Float64)
  prodA = (firstColor.x * ( 1.0 - percent ) ) + (secondColor.x * percent)
  prodB = (firstColor.y * ( 1.0 - percent ) ) + (secondColor.y * percent)
  prodC = (firstColor.z * ( 1.0 - percent ) ) + (secondColor.z * percent)

  product = SF.vector3f(prodA, prodB, prodC)
  product
end

def abs(num)
  num = num < 0 ? num * -1 : num
  num
end

class Triangle
  include SF::Drawable

  property position : SF::Vector2f
  property radius : Int32
  property scale = 1.0
  property texture : SF::RenderTexture

  def initialize(@radius : Int32, @position, tex : SF::RenderTexture)
    @texture = tex
  end

  def getGlobalBounds()
    pts = [] of Int32
    pts << @position[0].to_i
    pts << @position[1].to_i
    pts << @radius * 2
    pts << @radius * 2

    pts
  end

  def draw(target, states)
    triangle = SF::CircleShape.new(@radius, 3)
    triangle.scale = SF.vector2(@scale, @scale)
    triangle.set_position(@position.x, @position.y)
    triangle.texture = @texture.texture
    target.draw triangle, states
  end
end

class Tree
  include SF::Drawable

  property zoomPercent = 0
  property setForRemoval = false
  property slope = [] of Int32      # <- set to constants for now...

  def initialize(tex : SF::RenderTexture)
    c = SF.vector2f(0, 0)
    b = SF.vector2f(0, 50)
    d = SF.vector2f(50, 0)

    a = [] of SF::Vector2f
    a << c
    a << b
    a << d
    area = WINDOW_W * WINDOW_H // 25
    r    = Math.sqrt(area // 3).to_i

    @top = Triangle.new( r, SF.vector2f(0, 0), tex )
    @mid = Triangle.new( r, SF.vector2f(0, 0), tex )
    @bot = SF::RectangleShape.new(SF.vector2(r, r * 1.5))
    @bot.fill_color = SF.color(94, 47, 13)

    @bot.texture = tex.texture

    tmpPos = @top.position
    rad = @mid.radius
    @mid.position = SF.vector2(tmpPos[0], tmpPos[1] + 0.5 * rad)
    tmpPos = @mid.position
    @bot.position = SF.vector2(rad // 2, tmpPos[1] + 1.5 * rad)

    @scale = 1.0
  end

  def zoomIn(locale)
    moveVector = [] of Int32
    if locale == "field"
      #puts "field_tree_zoomed_in "
      moveVector << @slope[0]
      moveVector << @slope[1]
      @scale += 0.15
    elsif locale == "horizon"
      #puts "horizon_tree_zoomed_in "
      tmp = @top.position[0] < WINDOW_W // 2 ? -10 : 10
      moveVector << tmp
      moveVector << ((getGlobalBounds[3] // 3) * -1).as(Int32)
      @scale += 0.1
    end
    moveVector
  end

  # remember; up-and-out
  def zoomOut(locale)
    moveVector = [] of Int32
    if locale == "field"
      moveVector << (@slope[0] * -1)
      moveVector << (@slope[1] * -1)
      #@scale -= 0.15
      #puts "field_tree_zoomed_out "
    elsif locale == "horizon"
      #puts "horizon_tree_zoomed_out "
      tmp = @top.position[0] < WINDOW_W // 2 ? 10 : -10
      moveVector << tmp
      moveVector << (getGlobalBounds[3] // 3).as(Int32)
      #@scale -= 0.1
    end
    #puts moveVector
    moveVector
  end

  def zoom(dir)
    moveVector = [] of Int32

    if dir == "in_"
      @zoomPercent += 1
      moveVector = @zoomPercent <= ZOOM_HORIZON_THRESH ? zoomIn("horizon") : zoomIn("field")

    elsif dir == "out_"
      @zoomPercent -= 1
      if @zoomPercent <= ZOOM_HORIZON_THRESH
        moveVector = zoomOut("horizon")
        @scale -= 0.1                                   # had to take these out of zoomOut()...
      elsif @zoomPercent > ZOOM_HORIZON_THRESH
        moveVector = zoomOut("field")
        @scale -= 0.15                                  # but doing same to zoomIn()'s set caused huge scale problems...
      end
    end

    @top.scale = self.@scale
    @mid.scale = self.@scale
    @bot.scale = SF.vector2(@scale, @scale)

    #puts moveVector

    tmpArray = @top.getGlobalBounds()
    @top.position = SF.vector2f(tmpArray[0] + moveVector[0], tmpArray[1] + moveVector[1])
    tmpArray = @mid.getGlobalBounds()
    @mid.position = SF.vector2f(tmpArray[0] + moveVector[0], tmpArray[1] + moveVector[1])
    tmpArray = @mid.getGlobalBounds()
    rad = @mid.radius
    @bot.position = SF.vector2f(tmpArray[0] + rad * @scale // 2, tmpArray[1]  + rad * @scale)
    #puts @top.getGlobalBounds()
    if @zoomPercent >= ZOOM_THESH || @zoomPercent < 0
      @setForRemoval = true
    end
  end

  def getGlobalBounds()
    topPos = @top.position
    height = @top.radius * 2 + @mid.radius * 2 - (@mid.position[1] - @top.position[1]) + @bot.size[1] - (@bot.position[1] - @top.position[1])
    width  = @top.radius * 2

    pts = [] of Int32
    pts << topPos[0].to_i
    pts << topPos[1].to_i
    pts << width
    pts << height.to_i

    pts
  end

  def move(x : Int32, y : Int32)
    tmpPos = @top.position
    @top.position = SF.vector2(tmpPos[0] + x, tmpPos[1] + y)
    tmpPos = @mid.position
    @mid.position = SF.vector2(tmpPos[0] + x, tmpPos[1] + y)
    tmpPos = @bot.position
    @bot.position = SF.vector2(tmpPos[0] + x, tmpPos[1] + y)
  end

  def moveto(x : Int32, y : Int32)
    tmpPosT = @top.position
    tmpPosM = @mid.position
    tmpPosB = @bot.position
    @top.position = SF.vector2f(x, y)
    @mid.position = SF.vector2(tmpPosM[0] - tmpPosT[0] + x, tmpPosM[1] - tmpPosT[1] + y)
    @bot.position = SF.vector2(tmpPosB[0] - tmpPosM[0] + x, tmpPosB[1] - tmpPosM[1] + y)
  end

  def draw(target, states)
    painter = SF::Shader.from_file("shaders/vertex.vs", "shaders/tree.fs")
    states.shader = painter

    painter.set_parameter("desiredColor", 0.369, 0.184, 0.051)
    target.draw @bot, states

    painter.set_parameter("desiredColor", 0.137, 0.565, 0.137)
    target.draw @mid, states
    target.draw @top, states
  end

end

class Field
  include SF::Drawable

  property skyColor : SF::Vector3f
  property groundColor : SF::Vector3f
  property pathColor : SF::Vector3f
  property pPrevPercent = 0.0
  property prevPercent : Float64
  getter clockObj = SF::Clock.new

  def initialize(tex : SF::RenderTexture)
    @texture = tex

    @trees          = [] of Tree

    @skyColor     = SF.vector3f(0.0, 0.749, 1.0)
    @groundColor  = SF.vector3f(0.008, 0.678, 0.165)
    @pathColor    = SF.vector3f(0.565, 0.933, 0.56)

    @sky              = SF::RectangleShape.new(SF.vector2(WINDOW_W, WINDOW_H // 2))
    @ground           = SF::RectangleShape.new(SF.vector2(WINDOW_W, WINDOW_H // 2))
    @ground.position  = SF.vector2(0, WINDOW_H // 2)
    @path             = SF::ConvexShape.new
    @path.point_count = 4
    @path[0]          = SF.vector2(WINDOW_W // 2 - WINDOW_W // 8, WINDOW_H // 2)
    @path[1]          = SF.vector2(WINDOW_W // 2 + WINDOW_W // 8, WINDOW_H / 2)
    @path[2]          = SF.vector2(WINDOW_W // 2 + WINDOW_W // 4, WINDOW_H)
    @path[3]          = SF.vector2(WINDOW_W // 2 - WINDOW_W // 4, WINDOW_H)
    @fogBank          = SF::RectangleShape.new(SF.vector2(WINDOW_W, WINDOW_H // 2))

    @sky.texture      = tex.texture
    @ground.texture   = tex.texture
    @path.texture     = tex.texture
    @fogBank.texture  = tex.texture

    @prevPercent = abs(Math.sin(@clockObj.elapsed_time.as_seconds / 4.0))
    @sunUp = false
    @dayNightPeak = false
  end

  def add(t : Tree)
    @trees << t
  end

  def createTreeRow(zoomLevel, tex : SF::RenderTexture)
    t1 = Tree.new(tex)
    t2 = Tree.new(tex)
    t5 = Tree.new(tex)
    t6 = Tree.new(tex)

    t1Arr = t1.getGlobalBounds()
    t2Arr = t2.getGlobalBounds()
    t5Arr = t5.getGlobalBounds()
    t6Arr = t6.getGlobalBounds()

    #they're all at the same y level && same height
    #yPos = WINDOW_H / 2 - t1Arr[3]     <- this is y-level @ horizon thresh
    yPos = WINDOW_W // 2                 # <- this is ideally the starting y-level
    xPos = WINDOW_W // 2 - WINDOW_W // 8 - t1Arr[2]

    t1.@slope << -45
    t1.moveto(xPos.as(Int32), yPos.as(Int32))

    xPos = xPos - t2Arr[2]
    t2.@slope << -55
    t2.moveto(xPos.as(Int32), yPos.as(Int32))

    xPos = WINDOW_W // 2 + WINDOW_W // 8
    t5.@slope << 20
    t5.moveto(xPos.as(Int32), yPos.as(Int32))

    xPos = xPos + t5Arr[2]
    t6.@slope << 30
    t6.moveto(xPos.as(Int32), yPos.as(Int32))

    tmpZoom = zoomLevel
    arr = [t1, t2, t5, t6]

    arr.each do |treeObj|
      treeObj.@slope << 20
      while tmpZoom > 0
        treeObj.zoom "in_"
        tmpZoom -= 1
      end
      tmpZoom = zoomLevel
      add(treeObj)
    end
  end

  def hasPeaked(currentPercent : Float64)
    if abs(currentPercent) < @prevPercent && @prevPercent > @pPrevPercent
      @dayNightPeak = true
    end
  end

  def adjustLight()
    darkestSky    = SF.vector3f(0.0, 0.345, 0.463)
    lightestSky   = SF.vector3f(0.384, 0.847, 1.0)
    skyBase       = SF.vector3f(0.0, 0.749, 1.0)

    darkestGrnd   = SF.vector3f(0.039, 0.333, 0.082)
    lightestGrnd  = SF.vector3f(0.086, 0.749, 0.18)
    groundBase    = SF.vector3f(0.008, 0.678, 0.165)

    darkestPath   = SF.vector3f(0.565, 0.933, 0.56)
    lightestPath  = SF.vector3f(0.765, 0.965, 0.761)
    pathBase      = SF.vector3f(0.698, 0.953, 0.694)

    shadow        = SF.vector3f(0.0, 0.0, 0.0)
    highlight     = SF.vector3f(1.0, 1.0, 1.0)
    mixer = uninitialized SF::Vector3f

    dayNightPeak = false  #reset per-frame

    percent = Math.sin(@clockObj.elapsed_time.as_seconds / 4.0)
    hasPeaked(percent)

    if abs(percent) > @prevPercent && @prevPercent < @pPrevPercent
        if @sunUp == false
            @sunUp = true
            #print "one "
        else
            @sunUp = false
            #print "two "
        end
    end

    if ( (abs(percent) > @prevPercent && @prevPercent > @pPrevPercent) || (abs(percent) < @prevPercent && @prevPercent < @pPrevPercent) )
        if @sunUp == false
            @skyColor       = mix(skyBase.as(SF::Vector3f), shadow.as(SF::Vector3f), abs(percent))
            @groundColor    = mix(groundBase.as(SF::Vector3f), shadow.as(SF::Vector3f), abs(percent))
            @pathColor      = mix(pathBase.as(SF::Vector3f), shadow.as(SF::Vector3f), abs(percent))
            #print "three "
        else
            @skyColor       = mix(skyBase.as(SF::Vector3f), highlight.as(SF::Vector3f), abs(percent))
            @groundColor    = mix(groundBase.as(SF::Vector3f), highlight.as(SF::Vector3f), abs(percent))
            @pathColor      = mix(pathBase.as(SF::Vector3f), highlight.as(SF::Vector3f), abs(percent))
            #print "four "
        end
    end

    if @skyColor <= darkestSky
      @skyColor = darkestSky
      #print "fieve "
    elsif @skyColor >= lightestSky
      @skyColor = lightestSky
      #print "xsda "
    end

    if @groundColor <= darkestGrnd
      @groundColor = darkestGrnd
    elsif @groundColor >= lightestGrnd
      @groundColor = lightestGrnd
    end

    if @pathColor <= darkestPath
      @pathColor = darkestPath
    elsif @pathColor >= lightestPath
      @pathColor = lightestPath
    end

    @pPrevPercent = @prevPercent
    @prevPercent = abs(percent)
    #puts @skyColor
  end

  def createForest()
    createTreeRow(0, @texture)
    createTreeRow(5, @texture)
    createTreeRow(9, @texture)
  end

  def checkTreeZooms()
    flag = false
    tmpZoom = uninitialized Int32
    @trees.each do |tr|
      if tr.@setForRemoval == true
        tmpZoom = tr.@zoomPercent
        @trees.delete(tr)
        flag = true
      end # end if
    end # end for each

    if flag == true
      if tmpZoom >= ZOOM_THESH
        createTreeRow(0, @texture)
      elsif tmpZoom <= 0
        createTreeRow(ZOOM_THESH-1, @texture)
      end
    end
  end

  def sortTrees()
    @trees.sort_by! { |tr| tr.@zoomPercent }
  end

  def update()
    checkTreeZooms()
    sortTrees()
    adjustLight()
  end

  def draw(target, states)

    painter = SF::Shader.from_file("shaders/vertex.vs", "shaders/sky.fs");
    states.shader = painter
    painter.set_parameter("desiredColor", @skyColor.x, @skyColor.y, @skyColor.z)
    painter.set_parameter("smoothLowerBound", abs(Math.sin(@clockObj.elapsed_time.as_seconds // 4.0)) )
    if @dayNightPeak == true
      if @sunUp == false
        painter.set_parameter("shadowOrHilight", 0.0)
      elsif @sunUp == true
        painter.set_parameter("shadowOrHilight", 1.0)
      end
    end

    target.draw @sky, states

    # hackish, but ensures the trees appear 'below' the horizon
    @trees.each do |tree|
      if tree.@zoomPercent <= ZOOM_HORIZON_THRESH
        target.draw tree, states
      end
    end

    painter = SF::Shader.from_file("shaders/vertex.vs", "shaders/ground.fs")
    states.shader = painter
    painter.set_parameter("desiredColor", @groundColor.x, @groundColor.y, @groundColor.z)
    painter.set_parameter("shadowOrHilight", 1.0)
    target.draw @ground, states
    painter.set_parameter("desiredColor", @pathColor.x, @pathColor.y, @pathColor.z)
    target.draw @path, states

    painter = SF::Shader.from_file("shaders/vertex.vs", "shaders/fog.fs")
    states.shader = painter
    painter.set_parameter("time", @clockObj.elapsed_time.as_seconds)
    target.draw @fogBank, states

    @trees.each do |tree|
      if tree.@zoomPercent > ZOOM_HORIZON_THRESH
        target.draw tree, states
      end
    end
  end

end

window = SF::RenderWindow.new(SF::VideoMode.new(WINDOW_W, WINDOW_H), "My window")

states = SF::RenderStates.new

tex           = SF::RenderTexture.new

tex.create(WINDOW_W, WINDOW_H // 2 )
f1 = Field.new(tex)
f1.createForest

while window.open?

  while event = window.poll_event()
    if ( event.is_a? (SF::Event::Closed) || (event.is_a? (SF::Event::KeyPressed) && event.code.escape?) )
      window.close()
    elsif event.is_a? SF::Event::KeyPressed
      case event.code
        when .up?
          f1.@trees.each do |tree|
            tree.zoom "out_"
          end
        when .down?
          f1.@trees.each do |tree|
            tree.zoom "in_"
          end
      end    # end case
    end    # end if - elsif block
  end    # end while poll event

  f1.update

  window.clear SF::Color::Black
  window.draw f1, states
  window.display()

end # end while window open
