# shaders-test
An old project I cranked out to try and learn how glsl shaders can be used in applications.

<h2>Controls</h2>
Use the <strong>UP</strong> and <strong>DOWN</strong> arrow keys to 'scroll' through the scene. The <strong>ESC</strong> key will close the window. 

<p>
  <img src="screenshots/screen1.png" width="45%" alt="app with night-time colors" title="the forest path at 'night'" >
  <img src="screenshots/screen2.png" width="45%" alt="app with day-time colors"  title="the forest path" >
</p>
<quote>An example Day/Night transition</quote>

<h2>Requirements</h2>
<ol>
    <li>CrSFML - v 2.5.2+</li>
    <li>Crystal - v 0.35.1+</li>
    <li>SFML - v 2.5.0+</li>
</ol>

<h2>Getting it Running</h2>
<ul>
<li>Install Dependencies</li>
    
<li>Download the project</li>
<li>Run <code>shards install</code></li>
<li>Run <code>crystal main.cr</code></li>
</ul>
<h2>Credits</h2>
Crystal Lang - <a href="http://crystal-lang.org/">Website</a><br />
SFML - <a href="http://sfml-dev.org">Website</a><br />
CrSFML - <a href="https://github.com/oprypin/crsfml">Github</a>
